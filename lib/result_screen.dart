import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ResultScreen extends StatelessWidget {
  final bmiModel;

  ResultScreen({Key? key, this.bmiModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          padding: const EdgeInsets.all(32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              SizedBox(
                height: 200,
                width: 200,
                child: bmiModel.isNormal ?
                SvgPicture.asset(
                  "assets/images/happy.svg",
                  fit: BoxFit.contain,)
                    : SvgPicture.asset(
                  "assets/images/sad.svg",
                  fit: BoxFit.contain,) ,
              ),

              const SizedBox(
                height: 8,
              ),

              //แทรกข้อความ Your BMI is XXXX
              Text("Your BMI is ${bmiModel.bmi.round()}",
                style: TextStyle(color: Colors.red[700], fontWeight: FontWeight.w700, fontSize: 34),
              ),
              //แทรกข้อความ comments ใน bmiModel.comments
              Text("${bmiModel.comments}"),

              const SizedBox(height: 16,),

              //แทรกข้อความ Hurray! Your BMI is Normal. กรณีที่ bmiModel.isNormal เป็นจริง
              //หรือแทรกข้อความ Sadly! Your BMI is not Normal. กรณีที่ bmiModel.isNormal เป็นเท็จ
              //โดยใช้วิธีการกำหนดเงื่อนไขของ bmiModel.isNormal ดูตัวอย่าง บรรทัดที่ 24 ด้านบน
              bmiModel.isNormal ?
                  Text("Hurray! Your BMI is Normal.",
                    style: TextStyle(color: Colors.red[700], fontWeight: FontWeight.w500, fontSize: 18),
                  )
              : Text("Sadly! Your BMI is not Normal.",
                style: TextStyle(color: Colors.red[700], fontWeight: FontWeight.w500, fontSize: 18),
              ),

              const SizedBox(height: 16,),

              Container(
                child: ElevatedButton.icon(
                  onPressed: (){
                    //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.arrow_back_ios, color: Colors.white,),
                  label: const Text("LET CALCULATE AGAIN"),
                  //ปรับสีปุ่มให้เป็นสีชมพู
                  // style: ElevatedButton.styleFrom(primary: Colors.red[700]),
                ),
                width: double.infinity,
                padding: const EdgeInsets.only(left: 16, right: 16),
              )
            ],
          ),
        )
    );
  }
}
