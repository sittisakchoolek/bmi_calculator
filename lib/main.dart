import 'package:bmi_usethis/bmiModel.dart';
import 'package:bmi_usethis/result_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BMI_Calculator',
      theme: ThemeData(
        primarySwatch: Colors.pink,
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              primary: Colors.pink,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7),
              ),
              padding: const EdgeInsets.all(16),
              elevation: 5,
            ),
          )
      ),
      home: const MyHomePage(title: 'BMI_Calculator'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _heightOfUser = 100.0;
  double _weightOfUser = 40.0;
  var height = TextEditingController();
  var weight = TextEditingController();

  double _bmi = 0;
  // late String _bmiInString;

  late BMIModel _bmiModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 200,
                width: 200,
                child: SvgPicture.asset(
                  "assets/images/heart.svg",
                  fit: BoxFit.contain,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                "BMI Calculator",
                style: TextStyle(
                    color: Colors.red[700],
                    fontSize: 34,
                    fontWeight: FontWeight.w700),
              ),
              const Text(
                "We care about your health",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 15,
                    fontWeight: FontWeight.w400),
              ),
              const SizedBox(
                height: 32,
              ),
              const Text(
                "Height (cm)",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 24,
                    fontWeight: FontWeight.w400),
              ),
              Container(
                padding: const EdgeInsets.only(left: 16, right: 16),
                child: TextField(
                  controller: height,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp("[.0-9]")),
                  ],
                ),
              ),
              const SizedBox(
                height: 24,
              ),
              const Text(
                "Weight (kg)",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 24,
                    fontWeight: FontWeight.w400),
              ),
              Container(
                padding: const EdgeInsets.only(left: 16, right: 16),
                child: TextField(
                  controller: weight,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp("[.0-9]")),
                  ],
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Container(
                child: ElevatedButton.icon(
                  onPressed: () {
                    setState(() {
                      //แทรกโค้ดคำนวณ BMI และกำหนดเงื่อนไขให้แก่ตัวแปร _bmiModel
                      _heightOfUser = double.parse(height.text);
                      _weightOfUser = double.parse(weight.text);
                      _bmi = _weightOfUser / ((_heightOfUser/100) * (_heightOfUser/100));
                      // _bmiInString = _bmi.toStringAsFixed(2);
                      // _bmi = double.parse(_bmiInString);

                      if (_bmi >= 18.5 && _bmi < 25) {
                        _bmiModel = BMIModel(
                            bmi: _bmi,
                            isNormal: true,
                            comments: "You are Totaly Fit");
                      } else if (_bmi < 18.5) {
                        _bmiModel = BMIModel(
                            bmi: _bmi,
                            isNormal: false,
                            comments: "You are Underweighted");
                      } else if (_bmi > 25 && _bmi <= 30) {
                        _bmiModel = BMIModel(
                            bmi: _bmi,
                            isNormal: false,
                            comments: "You are Overrweighted");
                      } else {
                        _bmiModel = BMIModel(
                            bmi: _bmi,
                            isNormal: false,
                            comments: "You are Obesed");
                      }
                    });
                    //แทรกโค้ด Navigator.push เพื่อส่งค่า _bmiModel ไปยังหน้า ResultScreen
                    Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => ResultScreen(bmiModel: _bmiModel)
                      ),
                    );
                  },
                  icon: const Icon(
                    Icons.favorite,
                    // set icon และ สี icon ให้ถูกต้อง
                    color: Colors.white,
                  ),
                  label: const Text("CALCULATE"),
                  //ใส่สีปุ่ม เป็นสีชมพู
                  // style: ElevatedButton.styleFrom(primary: Colors.red[700]),
                ),
                width: double.infinity,
                padding: const EdgeInsets.only(left: 16, right: 16),
              ),
              Container(
                padding: const EdgeInsets.only(top: 100),
                child: const Text("Name: Sittisak Choolak\nStudent ID: 6450110013",
                style: TextStyle(color: Colors.amber,),),
              )
            ],
          ),
        ),
      ),
    );
  }
}
